FROM jupyter/datascience-notebook:latest
RUN conda install --yes rise ipyleaflet
COPY data/ /home/jovyan/data
COPY images/ /home/jovyan/images
COPY *.ipynb /home/jovyan/
