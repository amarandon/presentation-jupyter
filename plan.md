# Jupyter : un écosystème qui gagne à être connu

Visite guidée du système jovyen

# Je me présente

- 1995 : Bac L
- 2002 : première ligne de Python
- 2012 : presta pour Météo France
- 2018 : rejoint la startup WeatherForce

## Une petite démo pour poser le contexte

## Exécuter du code

### Visualiser des données

- texte
- tableaux
- visualisations graphiques
- visualisations interactives

## Ajouter des explications

- prose
- formules mathématiques

## Exporter un notebook

- le format .ipynb
- format imprimable
- script

### Autres fonctionalités de Jupyter

- gestionnaire de fichier
- terminal
- éditeur de texte

## Qu'est-ce qu'un notebook?

- une interface web
- une format de document

## À quoi ça sert ?

- expérimenter avec du code
- exploration de données
- présentation et partage
- enseignement

## Champs de recherche

- astronomie
- neuroscience
- linguistique
- météorologie
- marketing
- et bien d'autres...

## Quelques références

- https://www.nature.com/articles/d41586-018-07196-1
- https://www.techatbloomberg.com/blog/inside-the-collaboration-that-built-the-open-source-jupyterlab-project/
- https://www.theatlantic.com/science/archive/2018/04/the-scientific-paper-is-obsolete/556676/
- https://medium.com/netflix-techblog/notebook-innovation-591ee3221233

### JupyterLab

## Architecture de Jupyter

### Les kernels

#### Différents Kernels

R, Elixir, Go, C++

#### Kernel IPython

##### Commandes magiques

- documentation
- mesures de performance

##### Composants interactifs

- composants standards
- composants supplémentaires
- structurer une interface

## JupyterHub

Déployer Jupyter pour plusieurs utilisateurs

### Architecture de JupyterHub

### Points d'extension

- authentification
- gestion de fichiers
- lancement des serveurs

### Le lanceur Docker

- environement isolé pour chaque utilisateur
- fournir un environement pré-établi grace à une image

### Les images Docker existantes

- la hierarchie fournie par le projet Jupyter
- construire ses propres images

### Déployer à plus grande échelle

### Binder

### Vers des traitements de données reproductibles
